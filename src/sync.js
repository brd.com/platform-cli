import execa from 'execa';

async function sync() {
  const result = await execa('docker-machine', ['ssh', 'dinghy', 'sudo', 'date', '-u', '$(date -u +%m%d%H%M%Y)'],{
    cwd: './platform-content',
    shell: true,
  });
  if (result.failed) {
    return Promise.reject(new Error('Failed to sync dinghy'));
  }
  console.log(result.stdout);
  return;
}

export { sync }
