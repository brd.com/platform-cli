import { readdir, writeFile } from 'fs';
import inquirer from 'inquirer';
import execa from 'execa';

const storage = require('node-persist');

async function start(options) {
  const { localTunnel, partner } = await inquireMissing(options);
  let url;
  if(partner) {
    url = await startPartner({ partner, localTunnel });
  }
  storage.setItem('partner', partner);
  await writeConfig({ partner, url });
  const platformUrl = await startPlatform({ localTunnel });
  console.log('Platform running at:', platformUrl);
}

async function inquireMissing(options) {
  const availablePartners = await getPartners();
  if(options.partners && !options.params.length) {
    options = { ...(await inquirer.prompt({
      type: 'list',
      name: 'partner',
      message: 'Select a connector to start',
      choices: availablePartners,
      default: false,
    })), ...options };
  } else {
    options = {
      ...options,
      partner: options.params[0],
    };
  }
  
  return options;
}

async function getPartners() {
  let result = await new Promise((resolve, reject) => {
    readdir('.', function (err, filesPath) {
      if (err) reject(err);
      resolve(filesPath.map(function (filePath) {
        return filePath;
      }));
    });
  });
  result = result.filter(f => {
    return f.includes('partner-') && !f.includes('-connector');
  }).map(f => f.replace('partner-', ''));
  return result;
}

async function startPlatform({ localTunnel }) {
  try {
    console.log('Platform starting.')
    await execa('docker', ['compose', 'up', '-d'], {
      cwd: './platform-content',
    });
    let url = 'http://platform-content.breadwallet.docker';
    
    if(localTunnel) {
      try {
        const result = await execa('docker', ['compose', '-f', 'lt.docker-compose.yml', 'up', '-d'], {
          cwd: './platform-content',
          shell: true
        });
        console.log('Platform tunnel started.');
        const logs = await execa('docker', ['compose', '-f', 'lt.docker-compose.yml', 'logs'], {
          cwd: `./platform-content`,
        });
        url = getTunnelUrl(logs.stdout);
      } catch (e) {
        console.error(e);
      }
    }
    return url;
  } catch(e) {
    console.error('Problem starting platform.', e);
    return;
  }
}

function getTunnelUrl(stdout) {
  const logChunks = stdout.split("your url is: ");
  return logChunks[logChunks.length-1];
}

async function startPartner({ partner, localTunnel }) {
  try {
    const result = await execa('docker', ['compose', 'up', '-d'], {
      cwd: `./partner-${partner}`,
    });
    console.log(`Connector ${partner} started.`);
    let url = `http://partner-${partner}.brd.docker`;
    if(localTunnel) {
      try {
        const result = await execa('docker', ['compose', '-f', 'lt.docker-compose.yml', 'up', '-d'], {
          cwd: `./partner-${partner}`,
        });
        console.log('Connector tunnel started.');
        const logs = await execa('docker', ['compose', '-f', 'lt.docker-compose.yml', 'logs'], {
          cwd: `./partner-${partner}`,
        });
        url = getTunnelUrl(logs.stdout);
      } catch (e) {
        console.error(e);
      }
    }
    return url;
  } catch (e) {
    console.error(e);
    return;
  }
}

async function writeConfig({ url, partner }) {
  if(!url || !partner) return;
  writeFile('./platform-content/config.bpc.js', 
`let config = {
directPartnerUrl: "${url}/${partner}/rpc",
};

export default config;`, e => {
    console.log('config.bpc.js written to platform.');
  });
}

export { start }
