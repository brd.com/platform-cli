import execa from 'execa';

async function stop() {
  let result;
  try {
    result = await execa('dinghy', ['stop']);
  } catch (e) {
    console.log(e);
    return;
  }
}

export { stop }
