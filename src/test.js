import execa from 'execa';
const storage = require('node-persist');

async function test() {
  const partner = await storage.getItem('partner');
  runTest('./platform-content');
  if(partner) {
    runTest(`./partner-${partner}`);
  }
}

async function runTest(cwd) {
  try {
    const result = await execa.command('docker-compose exec -T app npm run test',{
      cwd,
      shell: true,
    });
    console.info(`test running for ${cwd}`, result.stdout);
  } catch(e) {
    console.error(e);
  }
}

export { test }
