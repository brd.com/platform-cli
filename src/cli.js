import arg from 'arg';
const storage = require('node-persist');

function parseArgumentsIntoOptions(rawArgs) {
 const args = arg(
   {
     '--local-tunnel': Boolean,
     '--partner': Boolean,
     '--follow': Boolean,
     '-t': '--local-tunnel',
     '-p': '--partner',
     '-f': '--follow',
   },
   {
     argv: rawArgs.slice(2),
   }
 );
 return {
   command: args._[0],
   params: args._.slice(1),
   partners: args['--partner'] || false,
   localTunnel: args['--local-tunnel'] || false,
   follow: args['--follow'] || false,
 };
}

export async function cli(args) {
  await storage.init();
  let options = parseArgumentsIntoOptions(args);
  options.command = options.command || 'start';
  if(options.command) {
    try {
      await (await import(`./${options.command}.js`))[options.command](options);
    } catch (e) {
      console.error(e);
    }
  }
}
