import { Gitlab } from '@gitbeaker/node';
const axios = require('axios');
const storage = require('node-persist');
import inquirer from 'inquirer';
import execa from 'execa';
// VsYjuxeNNdvzNJ2FvRAy

const STATE = ['opened', 'closed'];
function transformState(issue) {
  if(issue.state == STATE[1]) return 'Closed';
  if(issue.state == STATE[0] && issue.merge_requests_count) return 'In Progress';
  if(issue.assignee) return 'Assigned';
  return 'Unassigned';
}
async function handleAction(action) {
  if(!action) return;
  action = JSON.parse(action);
  if(action.type == "checkout") {
    return await execa('git', ['checkout', action.source_branch], {
      cwd: `./${action.project}`,
    });
    // git checkout branch
  }
}

async function tasks(options) {
  let token = await storage.getItem('gitlabToken');
  if(!token) {
    const { token: userToken } = await inquirer.prompt({
      type: 'input',
      name: 'token',
      message: 'Enter your personal access token from GitLab',
    });
    storage.setItem('gitlabToken', userToken);
    token = userToken;
  }
  const api = new Gitlab({
    token,
  });
  const projects = await api.Projects.all({ membership: true, simple: true });
  const issues = await api.Issues.all({ maxPages: 2, simple: true });
  let active = issues.reduce((list, { project_id, title, state, merge_requests_count, id }) => {
    if(state == STATE[1]) return list;
    let project = list.findIndex(i => i.value == project_id);
    if(project >= 0) {
      list[project].issues.push({ name: `${title} (${state})${(!!merge_requests_count && ' MR')}`, value: id });
    } else {
      const {  name, path } = projects.find(p => p.id == project_id);
      list.push({
        value: project_id,
        name,
        path,
        issues: [{ name: `${title} (${state})${(!!merge_requests_count && ' MR') || ''}`, value: id }],
      });
    }
    return list;
  }, []);
  const selectedProject = await inquirer.prompt({
    type: 'list',
    name: 'project_id',
    message: 'Active Projects',
    choices: active,
    default: false,
  });
  const project = active.find(p => p.value == selectedProject.project_id);
  const selectedIssue = (await inquirer.prompt({
    type: 'list',
    name: 'id',
    message: 'Active Issues',
    choices: project.issues,
    default: false,
  })).id;
  const issue = issues.find(i => i.id == selectedIssue);
  console.log(`${issue.title}`)
  console.log(`${issue.description}`);
  console.log(`Status: ${transformState(issue)}`);
  const actions = [{ name: 'Close', value: "" }];
  if(issue.merge_requests_count) {
    const mergeRequests = await axios.get(`https://gitlab.com/api/v4/projects/${project.value}/issues/${issue.iid}/related_merge_requests?private_token=${token}`);
    actions.splice(0, 0, {
      name: "Checkout MR",
      value: JSON.stringify({ type: 'checkout', source_branch: mergeRequests.data[0].source_branch, project: project.path })
    })
  }
  const action = await inquirer.prompt({
    type: "list",
    name: "action",
    message: "Select an action",
    choices: actions,
    default: false,
  });
  handleAction(action.action);
}

export { tasks }
