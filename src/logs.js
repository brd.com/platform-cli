import execa from 'execa';
import storage from 'node-persist';

async function logs(options) {
  const partner = await storage.getItem('partner');
  if(options.partners && partner) {
    return await printLog(`./partner-${partner}`, options);
  } else {
    return await printLog('./platform-content', options);
  }
}

async function printLog(cwd, { follow }) {
  try {
    let result;
    const params = ['logs'];
    const options = {
      cwd,
      shell: true,
    };
    console.log(`Printing logs for ${cwd}`);
    if(follow) {
      result = execa('docker-compose', [ ...params, '-f' ], options).stdout.pipe(process.stdout);
      return result;
    } else {
      result = (await execa('docker-compose', params, options)).stdout;
      console.log(result);
      return;
    }
  } catch(e) {
    console.error(e);
  }
}

export { logs }
