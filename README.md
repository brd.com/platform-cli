# BRD Platform CLI

### Start
```
-t localTunnel: starts up docker container for platform and connector tunnels
-p partner: starts a partner connector. optionally include partner name
```
#### Usage
Start Platform
```
$ bpc start
```
Start Platform with a Partner Connector and local tunnel
```
$ bpc start -tp [partner name e.g. 'wyre']
```

### Stop
#### Usage
Stop Dinghy
```
$ bpc stop
```

### Sync
#### Usage
Sync Dinghy Date
```
$ bpc sync
```

### Logs
```
-p partner: Prints logs for currently running partner connector
-f follow: Continuously prints logs
```
#### Usage
Print platform logs
```
$ bpc logs
```
Print partner logs continuously
```
$ bpc logs -pf
```

### Test
#### Usage

Run test on current environment
```
$ bpc test
```

### Tasks

#### Dependencies
Node v16
